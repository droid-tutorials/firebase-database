package br.com.ldrson.firebasedatabasetutorial;

/**
 * Created by leanderson on 27/10/16.
 */

public class Anotacao {

    private String uid;
    private String valor;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}
