package br.com.ldrson.firebasedatabasetutorial;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

import br.com.ldrson.firebasedatabasetutorial.firebase.AtualizarAnotacao;
import br.com.ldrson.firebasedatabasetutorial.firebase.DeletarAnotacao;
import br.com.ldrson.firebasedatabasetutorial.firebase.ObterNotas;
import br.com.ldrson.firebasedatabasetutorial.firebase.SalvarAnotacao;

public class MainActivity extends AppCompatActivity {

    private EditText mEditText;
    private TextView mTextViewCodigo;
    private TextView mTextViewAnotacao;

    private Anotacao mUltimaAnotacao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mEditText = (EditText) findViewById(R.id.edit_text);
        mTextViewCodigo = (TextView) findViewById(R.id.codigo_anotacao);
        mTextViewAnotacao = (TextView) findViewById(R.id.anotacao);


        //
        // Configurar o Firebase Database para ativar modo offline
        //
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);


        //
        // Ação do botão salvar
        //
        findViewById(R.id.bt_salvar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                salvarNovoRegistro();
            }
        });


        //
        // Ação do botão atualizar
        //
        findViewById(R.id.bt_atualizar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                atualizarRegistro();
            }
        });


        //
        // Ação do botão deletar
        //
        findViewById(R.id.bt_deletar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deletarRegistro();
            }
        });


        //
        // Busca as anotações do firebase
        //
        obterTodos();

    }

    private void deletarRegistro(){

        // Se nao for nulo

        if(mUltimaAnotacao != null){
            //
            // Manda deletar
            //
            DeletarAnotacao deletarAnotacao = new DeletarAnotacao();
            deletarAnotacao.deletar(mUltimaAnotacao);
            //
            // Recarregar lista
            //
            obterTodos();
        }
    }

    private void atualizarRegistro(){

        // Se nao for nulo

        if(mUltimaAnotacao != null){


            //
            // Atualiza o valor da anotação com base no que foi digitado no edittext
            //
            mUltimaAnotacao.setValor(mEditText.getText().toString());

            //
            // Manda deletar
            //
            AtualizarAnotacao atualizarAnotacao = new AtualizarAnotacao();
            atualizarAnotacao.atualizar(mUltimaAnotacao);
            obterTodos();
            //
            // Recarregar lista
            //
            obterTodos();
        }
    }

    private void salvarNovoRegistro() {

        //
        // Salva uma nova anotação
        //

        SalvarAnotacao salvar = new SalvarAnotacao();
        salvar.novoRegistro(mEditText.getText().toString());

        // Limpa o campo de texto
        mEditText.setText("");

        // Atualiza a lista
        obterTodos();
    }

    private void obterTodos(){
        ObterNotas obterNotas = new ObterNotas();
        obterNotas.todos(new ObterNotas.OnObterAnotacoesListener() {
            @Override
            public void onAnotacoesObtidas(List<Anotacao> lista) {

                //
                // Se existir anotacoes, pega a ultima e exibe em tela
                //
                if(lista.size() > 0){
                    mUltimaAnotacao = lista.get(lista.size() - 1);
                    exibirAnotacao();
                }else{
                    // Seta para nulo a ultima anotação
                    mUltimaAnotacao = null;
                    //
                    // Limpa os valores da tela
                    //
                    mTextViewAnotacao.setText("");
                    mTextViewCodigo.setText("");
                }

            }
        });
    }

    private void exibirAnotacao(){
        //
        // Exibe em tela as informações da anotação
        //
        mTextViewAnotacao.setText(mUltimaAnotacao.getValor());
        mTextViewCodigo.setText(mUltimaAnotacao.getUid());
    }


}
