package br.com.ldrson.firebasedatabasetutorial.firebase;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import br.com.ldrson.firebasedatabasetutorial.Anotacao;

/**
 * Created by leanderson on 27/10/16.
 */

public class DeletarAnotacao {


    public void deletar(Anotacao anotacao){

        //
        // Caminho da lista de notas
        //  /notas/código uui
        //
        DatabaseReference notasReferencia = FirebaseDatabase.getInstance().getReference()
                .child("notas").child(anotacao.getUid());

        //
        // Deleta do firebase setando o valor para nulo
        //
        notasReferencia.setValue(null);

    }

}
