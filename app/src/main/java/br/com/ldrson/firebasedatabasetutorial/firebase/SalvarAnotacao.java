package br.com.ldrson.firebasedatabasetutorial.firebase;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by leanderson on 27/10/16.
 */

public class SalvarAnotacao {


    public void novoRegistro(String texto){

        //
        // Caminho da lista de notas
        //  /notas
        //
        DatabaseReference notasReferencia = FirebaseDatabase.getInstance().getReference()
                .child("notas");

        //
        // Método push gera um novo registro
        //
        DatabaseReference novoRegistro = notasReferencia.push();
        novoRegistro.child("anotacao").setValue(texto);

    }

}
